package com.example.android.activitylifecycle;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.v("MainActivity","Creating Main Activity");
    }

    @Override
    protected void onStart(){
        super.onStart();
        Log.v("MainActivity","Starting Main Activity");

    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.v("MainActivity","Resuming Main Activity");

    }
    @Override
    protected void onPause(){
        super.onPause();
        Log.v("MainActivity","Pausing Main Activity");

    }
    @Override
    protected void onStop(){
        super.onStop();
        Log.v("MainActivity","Stopping Main Activity");

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.v("MainActivity","Restarting Main Activity");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.v("MainActivity","Destroying Main Activity ");
    }
}
